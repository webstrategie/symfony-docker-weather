<?php

namespace App\Service;

use App\Entity\Weather;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

class WeatherService
{
    /**
     * @var Configuration
     */
    private $config;

    /**
     * @var Client
     */
    private $Client;

    /**
     * Configuration constructor.
     * @param array $configuration
     */
    public function __construct(WeatherConfiguration $config)
    {
        $this->config = $config;
        $this->client = $config->getHttpClient();
    }

    /**
     * @param string $cityName
     * @return ResponseInterface
     */
    public function getByCityName($cityName)
    {
        return $this->request(['location' => $cityName]);
    }


    /**
     * @param array $parameters
     * @return ResponseInterface
     */
    private function request(array $parameters)
    {
        $parameters['format'] = 'json';
        $link  = $this->config->baseUri() . '?' . http_build_query($parameters);
        $response = $this->client->get($link, ['auth' => 'oauth']);
        return $this->response($response->getBody());
    }


    /**
     * @param array $data
     * @return mixed
     */
    public function response($data)
    {
        $result =  json_decode($data);

        $weather = new Weather();
        $weather->setLocation($result->location->city);
        $weather->setHumidity($result->current_observation->atmosphere->humidity);
        $weather->setPressure($result->current_observation->atmosphere->pressure);
        $weather->setWindSpeed($result->current_observation->wind->speed);
        $weather->setWindDirection($result->current_observation->wind->direction);
        $weather->setTemperature($result->current_observation->condition->temperature);

        return $weather;
    }
}
