<?php

namespace App\Entity;


/**
 * Entity Weather
 * 
 */
class Weather
{
    /**
     * @var string
     */
    private $location;

    /**
     * @var string
     */
    private $pressure;

    /**
     * @var int
     */
    private $temperature;

    /**
     * @var string
     */
    private $humidity;

    /**
     * @var int
     */
    private $windDirection;

    /**
     * @var float
     */
    private $windSpeed;

    /**
     * Set Location
     *
     * @param string $location
     * @return Weather
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * Get Location
     *
     * @return string $location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set Weather date
     *
     * @param string $pressure
     * @return Weather
     */
    public function setPressure($pressure)
    {
        $this->pressure = $pressure;
        return $this;
    }

    /**
     * Get Weather date
     *
     * @return string $pressure
     */
    public function getPressure()
    {
        return $this->pressure;
    }

    /**
     * Set Temperature
     *
     * @param int $temperature
     * @return Weather
     */
    public function setTemperature($temperature)
    {
        $this->temperature = $temperature;
        return $this;
    }

    /**
     * Get Temperature
     *
     * @return int $temperature
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * Set Humidity
     *
     * @param string $humidity
     * @return Weather
     */
    public function setHumidity($humidity)
    {
        $this->humidity = $humidity;
        return $this;
    }

    /**
     * Get Humidity
     *
     * @return string $humidity
     */
    public function getHumidity()
    {
        return $this->humidity;
    }

    /**
     * Set WindDirection
     *
     * @param int $windDirection
     * @return Weather
     */
    public function setWindDirection($windDirection)
    {
        $this->windDirection = $windDirection;
        return $this;
    }

    /**
     * Get WindDirection
     *
     * @return int $windDirection
     */
    public function getWindDirection()
    {
        return $this->windDirection;
    }


    /**
     * Set WindSpeed
     *
     * @param float $windSpeed
     * @return Weather
     */
    public function setWindSpeed($windSpeed)
    {
        $this->windSpeed = $windSpeed;
        return $this;
    }

    /**
     * Get WindSpeed
     *
     * @return float $windSpeed
     */
    public function getWindSpeed()
    {
        return $this->windSpeed;
    }
}
