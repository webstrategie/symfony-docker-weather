FROM php:7.3.0-apache

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && apt-get update && apt-get install -y git libzip-dev unzip \
    && docker-php-ext-install zip \
    && a2enmod rewrite headers

copy . /var/www/html

WORKDIR /var/www/html/symfony-weather

RUN usermod -u 1000 www-data
RUN chown -R www-data:www-data /var/www/html/symfony-weather/var/cache
RUN chown -R www-data:www-data /var/www/html/symfony-weather/var/log

RUN composer install